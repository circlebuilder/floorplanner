# Contributors

Many thanks to the following people for contributing to this website:

<!-- Add contributors alphabetically. -->

- Aravinth Manivannan, @realaravinth ([@realaravinth](https://gts.batsense.net/@realaravinth) on Fediverse)
- Arnold Schrijver, @circlebuilder ([@humanetech](https://mastodon.social/@humanetech) on Fediverse)
