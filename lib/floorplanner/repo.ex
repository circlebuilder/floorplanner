defmodule Floorplanner.Repo do
  use Ecto.Repo,
    otp_app: :floorplanner,
    adapter: Ecto.Adapters.Postgres
end
