defmodule FloorplannerWeb.PageController do
  use FloorplannerWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
