<div align="center">

[![floorplanner logo](assets/images/floorplanner-logo.svg)](https://docs.solidground.work)

### [Project docs](https://docs.solidground.work/floorplanner/project) | [Matrix space](https://matrix.to/#/#solidground-matters:matrix.org) | [Discussion forum](https://discuss.coding.social/c/solidground/13)

</div>

<br>

Floorplanner is an online design and automation toolkit that aims to make it as easy as possible
to create and extend applications for the Fediverse. Guiding you along the creation process
Floorplanner shields you from complexity inherent to federated apps. So that you can focus on the
tasks at hand and deliver features that people need. With Floorplanner you don't just develop
apps, you **create social experiences**.

## Contents

- [Why Floorplanner](#overview)
- [Getting started](#getting-started)
- [Contributing](#contributing)
- [Community](#community)
- [Resources](#resources)
- [License](#license)

## Why Floorplanner

Floorplanner helps focus beyond the code and address all aspects of the solution design.

### Social experience design (SX)

Floorplanner's primary goal is for everyone involved in the solution design to 'think beyond the app'.
Create focus on common understanding and finding people's true needs. Social aspects drive the process.

### Practice social coding

Floorplanner follows an inclusive approach to software development that involves everyone in the 
creation process, recognizes their skills, and provides optimal conditions for collaboration.

### Provide process guidance

Floorplanner offers a growing list of Process Recipes to guide creators through important tasks.
Recipes contain best-practices, background information, step-by-step procedures and checklists.

### Joyful creation

Floorplanner aims to take away the chores. Process Recipes are improved and automated over time,
so that creators can focus more on the fun parts: to deliver the features that clients need.

## Getting started

### Prequisites

- Erlang/OTP v25.1 or higher.
- Elixir v1.14 or higher.
- Fork of [Floorplanner repository](https://codeberg.org/solidground/floorplanner).

### Launching the app

_(TODO)_

To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.setup`
  * Start Phoenix endpoint with `mix phx.server` or inside IEx with `iex -S mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

Ready to run in production? Please [check our deployment guides](https://hexdocs.pm/phoenix/deployment.html).

## Contributing

_(TODO)_

### Code of conduct

We practice [Social Coding Principles](https://coding.social/principles). By participating in our
projects you consent to our [Community Participation Guidelines](CODE_OF_CONDUCT.md).

### Credits

We are most thankful to our [Contributors](CONTRIBUTORS.md) who helped build this project. Please do not forget to add yourself to this list if you are one of them.

## Resources

_(TODO)_

  * Official website: https://www.phoenixframework.org/
  * Guides: https://hexdocs.pm/phoenix/overview.html
  * Docs: https://hexdocs.pm/phoenix
  * Forum: https://elixirforum.com/c/phoenix-forum
  * Source: https://github.com/phoenixframework/phoenix

## License

Copyright (c) 2022 [Solidground](https://solidground.work) and contributors.

Floorplanner is licensed under [GNU Affero General Public License v3.0](LICENSE).