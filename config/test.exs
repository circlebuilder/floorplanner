import Config

# Only in tests, remove the complexity from the password hashing algorithm
config :bcrypt_elixir, :log_rounds, 1

# Configure your database
#
# The MIX_TEST_PARTITION environment variable can be used
# to provide built-in test partitioning in CI environment.
# Run `mix help test` for more information.
db_host =
  case System.get_env("DB_HOST") do
    nil -> "localhost"
    val -> val
  end

IO.puts("Using database host: #{db_host}")

config :floorplanner, Floorplanner.Repo,
  username: "postgres",
  password: "password",
  hostname: db_host,
  database: "floorplanner_test#{System.get_env("MIX_TEST_PARTITION")}",
  pool: Ecto.Adapters.SQL.Sandbox,
  pool_size: 10

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :floorplanner, FloorplannerWeb.Endpoint,
  http: [ip: {127, 0, 0, 1}, port: 4002],
  secret_key_base: "slcayUScgnFzUjhuVLbpVGVH6O4IZLPDou0OtdqGT5pQb0wGkzxEQhqIRwn/+bJz",
  server: false

# In test we don't send emails.
config :floorplanner, Floorplanner.Mailer, adapter: Swoosh.Adapters.Test

# Print only warnings and errors during test
config :logger, level: :warn

# Initialize plugs at runtime for faster test compilation
config :phoenix, :plug_init_mode, :runtime
